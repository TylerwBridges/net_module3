﻿using Carlookup.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace CarLookup.Services.Interface
{
    public interface MockServiceInterface
    {
        ICollection<Car> GetAll();
    }
}
