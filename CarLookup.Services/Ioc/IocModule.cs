﻿using Autofac;

namespace CarLookup.Services
{
    public class IocModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterModule(new Core.IocModule());

            builder.RegisterType<Data.MockRepository>().As<Data.MockRepository>();

        }
    }
}
