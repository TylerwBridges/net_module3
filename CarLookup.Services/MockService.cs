﻿using System;
using System.Collections.Generic;
using System.Text;
using Carlookup.Models;
using CarLookup.Data;
using CarLookup.Data.Interfaces;
using CarLookup.Services.Interface;

namespace CarLookup.Services
{
    public class MockService : MockServiceInterface
    {
        private IMockRepository mockRepo;

        public MockService(MockRepository _mockRepo)
        {
            mockRepo = _mockRepo;
        }

        public ICollection<Car> GetAll()
        {
            return mockRepo.Cars;
        }
    }
}
