﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Carlookup.Models
{
    public class Car
    {

        public int Id { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public int Year { get; set; }

        public Car(int Id, string Make, string Model, int Year)
        {
            this.Id = Id;
            this.Make = Make;
            this.Model = Model;
            this.Year = Year;
        }

        public Car getById(int id, List <Car> cars)
        {
            Car car = cars.Where(c => c.Id.Equals(id)).Select(c => c).FirstOrDefault();

            return car;
        }
    }
}