﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Carlookup.Models
{
    public class LinqAndLambda
    {
        public List<Car> _cars;

        public LinqAndLambda()
        {
            _cars = new List<Car>();

            _cars.Add(new Car(0, "Honda", "Civic", 2016));

            _cars.Add(new Car(1, "Honda", "Accord", 2018));

            _cars.Add(new Car(2, "Honda", "Ridgeland", 2017));

            _cars.Add(new Car(3, "Toyota", "Corolla", 2016));

            _cars.Add(new Car(4, "Toyota", "Camry", 2018));

        }


    }
}