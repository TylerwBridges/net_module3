﻿using System;
using System.Collections.Generic;
using System.Text;
using Autofac;
namespace CarLookup.Data
{
     public class IocModule : Module
     {
         protected override void Load(ContainerBuilder builder)
         {
             builder.RegisterModule(new Core.IocModule());

             builder.RegisterType<MockRepository>().As<MockRepository>();

         }
     }
}
