﻿using System.Collections.Generic;
using Carlookup.Models;
using CarLookup.Data.Interfaces;

namespace CarLookup.Data
{
    public class MockRepository : IMockRepository
    {
        private ICollection<Car> cars;

        public MockRepository()
        {
            cars = new List<Car>()
            {
                {new Car(0, "Honda", "Civic", 2016)},
                {new Car(1, "Honda", "Accord", 2018)},
                {new Car(2, "Honda", "Ridgeland", 2017)},
                {new Car(3, "Toyota", "Corolla", 2016)},
                {new Car(4, "Toyota", "Camry", 2018) }
            };
        }

        public ICollection<Car> Cars
        {
            get { return cars; }
            set { cars = value; }
        }

    }
}
