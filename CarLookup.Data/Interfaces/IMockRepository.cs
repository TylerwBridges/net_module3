﻿using System;
using System.Collections.Generic;
using System.Text;
using Carlookup.Models;

namespace CarLookup.Data.Interfaces
{
    public interface IMockRepository
    {
        ICollection<Car> Cars { get; set; }
    }
}
