﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Carlookup.Models
{
    public class CarList
    {
        public CarList()
        {
             List<Car> list = new List<Car>();

            list.Add(new Car()
            {
                Id = 0,
                Make = "Honda",
                Model = "Civic",
                Year = 2001,
            });

            list.Add(new Car()
            {
                Id = 1,
                Make = "Honda",
                Model = "Accord",
                Year = 2012,
            });

            list.Add(new Car()
            {
                Id = 2,
                Make = "Honda",
                Model = "Ridgeland",
                Year = 2017,
            });

            list.Add(new Car()
            {
                Id = 3,
                Make = "Honda",
                Model = "Oddessy",
                Year = 2016,
            });

            list.Add(new Car()
            {
                Id = 4,
                Make = "Honda",
                Model = "Civic",
                Year = 2018,
            });

        }
    }
}