﻿using Carlookup.Models;
using System;
using System.Collections.Generic;
using System.Drawing.Text;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using CarLookup.Data;
using CarLookup.Data.Interfaces;
using CarLookup.Services;
using CarLookup.Services.Interface;

namespace Carlookup.Controllers
{
    public class CarsController : ApiController
    {
        private MockServiceInterface mockService;
        private MockRepository repo;


        // GET
        //Returns all Cars
        [HttpGet]
        [Route("api/getCars")]
        public HttpResponseMessage getCars() 
        {

            mockService = new MockService(repo).GetAll();
        
            ICollection<Car> cars = mockService.GetAll();
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, cars);

            return response;
        }

        // POST
        [HttpPost]
        [Route("api/postCars")]
        public HttpResponseMessage postCars([FromBody]Car value)
        {
            mockService = new MockService();

            ICollection<Car> cars = mockService.GetAll();

            cars.Add(new Car(value.Id, value.Make, value.Model, value.Year));
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created, cars);
            return response;
        }

        // PUT
        [HttpPut]
        [Route("api/PutCars")]
        public HttpResponseMessage PutCars([FromBody]Car value)
        {
            mockService = new MockService();

            ICollection<Car> cars = mockService.GetAll();

            Car car = cars.Where(c => c.Id.Equals(value.Id)).Select(c => c).FirstOrDefault();

            cars.Remove(car);
            cars.Add(value);

            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Accepted, cars);
            return response;
        }

        // DELETE
        [HttpDelete]
        [Route("api/deleteCars")]
        public HttpResponseMessage DeleteCars([FromBody] Car value)
        {
            mockService = new MockService();

            ICollection<Car> cars = mockService.GetAll();


            cars.Remove(value);
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.NoContent, cars);
            return response;
        }

    }

}

