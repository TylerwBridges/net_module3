﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using Autofac;
using Autofac.Integration.Mvc;
using Autofac.Integration.WebApi;
using CarLookup.Services;

namespace Carlookup.Ioc
{
    public class AutofacConfig
    {
        private static IContainer container;

        public static void Configure()
        {
            var builder = new ContainerBuilder();
            builder.RegisterModule(new IocModule());

            container = builder.Build();

            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));

            GlobalConfiguration.Configuration.DependencyResolver = new AutofacWebApiDependencyResolver(container);
        }

        public static T getContainer<T>()
        {
            return (T)container;

        }

        public static T Resolve<T>() where T: class
        {
            return container.Resolve<T>();
        }

    }
}